# useR! 2019 Poster
Poster made for the useR! Conference wich was held in Toulouse, 9-12 July 2019.

[Poster (png)](https://gitlab.ifremer.fr/nr22372/user2019-poster/blob/master/poster.png)

[Flash presentation slide](https://gitlab.ifremer.fr/nr22372/user2019-poster/blob/master/Raillard-I53.pdf)

